package main

import (
	"flag"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
)

var numFrogs = flag.Int("numFrogs", 5000, "the number of frogs")
var move = flag.Bool("move", true, "")
var turtlePreference = flag.Float64("turtlePreference", 0.3, "")
var frogPreference = flag.Float64("frogPreference", 0.3, "")

type Frog struct {
}

func (sm *Frog) Setup(env *core.Environment, t *core.Turtle) {
	// Do nothing
	t.SetAttribute("type", "frog")
	t.SetColor(color.RGBA{0, 255, 255, 0})
}

func (sm *Frog) Run(env *core.Environment, t *core.Turtle) {
	t.FollowGradient(env, 1, 1, "frog-scent")
	t.Wiggle()
	amountTurtle := t.AmountAll(env, 1, "turtle-scent")
	amountFrog := t.AmountAll(env, 1, "frog-scent")
	if amountFrog > 1 && amountFrog > (amountTurtle+amountFrog)*float32(*frogPreference) {
		// Do Nothing
	} else {
		if *move {
			t.Step(env)
		}
	}
	t.Drop(env, 1, "frog-scent")
}

type Turtle struct {
}

func (sm *Turtle) Setup(env *core.Environment, t *core.Turtle) {
	// Do nothing
	t.SetAttribute("type", "turtle")
	t.SetColor(color.RGBA{255, 000, 0, 0})
}

func (sm *Turtle) Run(env *core.Environment, t *core.Turtle) {
	t.FollowGradient(env, 1, 1, "turtle-scent")
	t.Wiggle()
	amountTurtle := t.AmountAll(env, 1, "turtle-scent")
	amountFrog := t.AmountAll(env, 1, "frog-scent")
	if amountTurtle > 1 && amountTurtle > (amountTurtle+amountFrog)*float32(*turtlePreference) {
		// Do Nothing
	} else {
		if *move {
			t.Step(env)
		}
	}
	t.Drop(env, 1, "turtle-scent")
}

func mainrun() {
	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()
	num := -1
	experiment.InitNTurtles(func() core.Actor {
		num++
		if num%2 == 0 {
			sm := new(Frog)
			return sm
		}
		return new(Turtle)
	}, *numFrogs+experiment.GetNumTurtles())

	experiment.InitPheromone("turtle-scent", color.RGBA{0xFF, 0x00, 0x00, 0x00})
	experiment.InitPheromone("frog-scent", color.RGBA{0x00, 0xFF, 0xFF, 0x00})

	experiment.OnStep = func(env *core.Environment, turtles []*core.Turtle, i int) {
		env.Evaporate(.99, "turtle-scent")
		env.Evaporate(.99, "frog-scent")
	}

	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
