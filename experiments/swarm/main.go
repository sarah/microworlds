package main

import (
	"flag"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"git.openprivacy.ca/sarah/microworlds/models"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
	"math"
)

var sniffDistance = flag.Int("sniffDistance", 15, "the distance an ant can detect pheromone levels from")
var dropSize = flag.Float64("dropSize", 1.0, "the amount of pheromone an ants drops")

type Wall struct {
}

func (a *Wall) Setup(env *core.Environment, t *core.Turtle) {
	t.SetColor(color.RGBA{0x4b, 0x35, 0x57, 0xff})
}
func (a *Wall) Run(env *core.Environment, t *core.Turtle) {

}

type SlimeMold struct {
	Num            int
	StartX, StartY int
	Energy         float64
	Count          float32
}

func (sm *SlimeMold) Setup(env *core.Environment, t *core.Turtle) {
	t.SetColor(color.RGBA{0x1f, 0xff, 0x00, 0xff})
	//t.SetXY(sm.StartX, sm.StartY)
	sm.Energy = 0
}

func (sm *SlimeMold) CheckNeighbours(env *core.Environment, ox, oy int) int {
	neighbours := 0
	if env.Check(ox-1, oy-1) {
		neighbours++
	}
	if env.Check(ox, oy-1) {
		neighbours++
	}
	if env.Check(ox+1, oy-1) {
		neighbours++
	}
	if env.Check(ox-1, oy) {
		neighbours++
	}
	if env.Check(ox+1, oy) {
		neighbours++
	}

	if env.Check(ox-1, oy+1) {
		neighbours++
	}
	if env.Check(ox, oy+1) {
		neighbours++
	}
	if env.Check(ox+1, oy+1) {
		neighbours++
	}
	return neighbours
}

func (sm *SlimeMold) Run(env *core.Environment, t *core.Turtle) {
	neighbour := t.Check(env)
	if neighbour != nil {
		n, ok := neighbour.GetActor().(*SlimeMold)
		if ok && n.Energy > sm.Energy {
			sm.Energy = (sm.Energy + n.Energy) * .5
			n.Energy = (sm.Energy + n.Energy) * .5
		}
	}

	// Move around the world, if there are too many slimes around us turn around, otherwise follow slimes and food.
	t.Wiggle()

	if t.Amount(env, 1, "slime") > 10 {
		t.FollowGradient(env, 1, 10, "slime")
		t.TurnAround()
	}
	t.FollowGradient(env, 1, .1, "food")
	// If we have no neighbours we pretend we found some food so the others can find us.
	t.Step(env)
	ox, oy := t.Pos()
	neighbours := sm.CheckNeighbours(env, ox, oy)
	if neighbours > 2 {
		sm.Count = 0
		t.Drop(env, .1, "slime")
		if neighbours >= 7 {
			t.Wiggle()
			t.Wiggle()
			t.Wiggle()
			t.Drop(env, 1, "slime")
		}
	} else {
		sm.Count += 0.1
		t.Drop(env, float32(math.Pow(2, float64(sm.Count))), "food")
	}

	// We've found food, let's drop some chemical to tell others
	if env.HasValue(t.Pos()) {
		//env.TakeValue(t.Pos())
		//env.TakeValue(t.Pos())
		t.Drop(env, 255, "food")
		sm.Energy = 256
	}

	if sm.Energy > 0 {
		sm.Energy -= 2
		if sm.Energy == 0 {
			//t.SetAttribute("status","dead")
		}
	}
	t.Drop(env, float32(sm.Energy), "food")
	t.SetColor(color.RGBA{100, uint8(sm.Energy / 256), 10, 0})

}

var W = 1

var maze0 = [][]int{
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, 0, 0, 3, 0, 3, 0, 3, 0, 0, 0, 3, 0, 3, W},
	{W, 0, W, W, W, W, W, 3, W, 3, W, 0, W, 0, W},
	{W, 0, W, 0, 3, 0, 3, 0, W, 0, W, 0, W, 3, W},
	{W, 3, W, 3, W, W, W, W, W, 3, W, 3, W, 0, W},
	{W, 0, W, 0, W, 0, 3, 0, 3, 0, W, 0, W, 3, W},
	{W, 3, W, 3, W, 0, W, W, W, 3, W, 3, W, 0, W},
	{2, 2, 3, 0, W, 3, 0, 0, W, 0, W, W, W, 0, W},
	{2, 2, W, 0, W, W, 0, W, W, 3, W, 3, W, 3, W},
	{2, 2, W, 0, W, 0, 3, W, 0, 0, W, 0, W, 0, W},
	{2, 2, W, 3, W, W, 0, W, 3, W, W, 3, 0, 3, W},
	{2, 2, W, 0, W, 0, 3, W, 0, 3, 0, 0, W, 0, W},
	{2, 2, W, 3, W, 3, W, W, W, W, W, 3, W, 3, W},
	{2, 2, W, 0, W, 0, 0, W, 2, 2, 2, 2, W, 0, W},
	{W, W, W, W, W, W, W, W, 2, 2, 2, 2, W, W, W},
}

var maze2 = [][]int{
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, 2, 2, 2, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, 2, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, 2, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, 2, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, 2, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, 3, 3, 3, W, W, W, W, W, W},
	{W, W, W, W, W, W, 3, 3, 3, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
}

var logo = [][]int{
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, 2, 2, 2, 2, 2, W, 2, W, 2, 2, W, 2, 2, W},
	{W, 2, W, 2, W, 2, W, 2, W, 2, W, W, 2, W, W},
	{W, 3, W, 2, W, 2, W, 3, W, 2, 3, W, 3, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, 3, 2, 2, W, 3, W, W, W, 2, W, 3, 2, 2, W},
	{W, 2, W, 2, W, 2, W, 2, W, 2, W, 2, W, 2, W},
	{W, 2, 2, 2, W, 2, 2, 2, 2, 2, W, 2, 2, 2, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, 2, 2, W, 2, W, W, 2, 2, W, W, 2, 5, W, W},
	{W, 2, W, W, 2, W, W, 2, 4, 5, W, 2, W, W, W},
	{W, 3, W, W, 2, 3, W, 3, 2, W, 4, 3, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
}

var maze = [][]int{
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 2, 2, 0, 2, 2, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 2, 2, 0, 2, 2, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 2, 2, 0, 2, 2, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 2, 2, 0, 2, 2, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
}

var smallmaze = [][]int{
	{W, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, W},
	{W, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, W},
	{W, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, 2, 2, W},
	{W, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, W, W, W, W},
	{W, 2, W, 2, 2, W, W, W, W, W, 2, 2, W, 0, W},
	{W, 2, W, W, 2, W, 3, 3, 3, W, 2, 2, W, 0, W},
	{W, 2, W, 2, 2, 2, 3, 0, 3, W, W, 0, W, 0, W},
	{W, 2, W, W, W, W, 3, 0, 3, W, 0, 0, W, 0, W},
	{W, 2, 2, 2, 2, W, 3, 3, 3, W, 0, 0, 0, 0, W},
	{W, 2, 2, 2, 2, W, W, W, W, W, 0, 0, 0, 0, W},
	{W, 2, 2, 2, W, W, W, W, W, W, W, W, W, W, W},
	{W, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, W},
	{W, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, W},
	{W, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, W},
}

var steiner2 = [][]int{
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 2, 2, 0, 2, 2, 0, 2, 2, 0, 0, W},
	{W, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 2, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
}

var mazetest = [][]int{
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, W, W, W, W, W, 0, W, 0, W, 0, 0, W},
	{W, 0, 0, 0, 0, W, 0, W, 0, W, 0, W, 0, 0, W},
	{W, 0, 0, 0, 0, W, 3, W, 2, 2, 0, W, 0, 0, W},
	{W, 0, 0, 0, 0, W, 0, 0, 2, 2, 2, 2, 2, 2, W},
	{W, 0, 0, 0, 0, 2, 2, W, 2, 2, 0, 0, 0, 2, W},
	{W, 0, 0, 0, 0, 2, 2, W, 0, 0, 0, 0, 0, 2, W},
	{W, 0, 0, 0, 0, 2, 2, W, 0, 0, 0, 0, 0, 2, W},
	{W, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 2, W},
	{W, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 2, W},
	{W, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 2, W},
	{W, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 2, W},
	{W, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
}

var mold = [][]int{
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 2, 2, 0, 0, 0, 2, 2, 0, 0, W},
	{W, 0, 0, 0, 0, 2, 2, 2, 0, 2, 2, 2, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
}

var utrap = [][]int{
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 2, W, 0, 2, 0, W, 2, 0, 0, 0, W},
	{W, 0, 0, 0, 0, W, 0, 0, 0, W, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 2, W, W, W, W, W, 2, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 2, 0, 3, 0, 0, 2, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, W},
	{W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
}

func mainrun() {

	//maze = maze0

	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()
	t := new(core.Turtle)
	t.SetColor(color.RGBA{255, 0, 0, 255})
	experiment.InitEnvironment(func(environment *core.Environment) {
		// Create 2 food piles

		for x := 0; x < 300; x++ {
			for y := 0; y < 300; y++ {
				if maze[y/20][x/20] == 1 {
					//if ((x %20 < 10) && (y% 20<10))  || x < 30 || y < 30 || y > 270 || x > 270 || (x <100 && y>200) || ((x>150 && x < 300) && (y > 150 && y<175))  || ((x>150 && x < 180) && (y > 100 && y<175)){
					environment.Occupy(t, x, y)
				} else if maze[y/20][x/20] == 2 {
					environment.PutValue(x, y)
				} else if maze[y/20][x/20] == 4 {
					if x%20 > 10 {
						environment.PutValue(x, y)
					} else {
						environment.Occupy(t, x, y)
					}
				} else if maze[y/20][x/20] == 5 {
					if x%20 < 10 {
						environment.PutValue(x, y)
					} else {
						environment.Occupy(t, x, y)
					}
				}
				///	}
			}
		}
	})

	for x := 0; x < 300; x++ {
		for y := 0; y < 300; y++ {
			if maze[y/20][x/20] == 3 {
				i := 0
				experiment.InitNTurtles(func() core.Actor {
					sm := new(models.NeoSlimeMold)
					//sm.SniffDistance = *sniffDistance
					sm.StartX, sm.StartY = x, y
					sm.Num = i
					i++
					return sm
				}, 1)
			}
		}
	}

	experiment.InitNTurtles(func() core.Actor {
		sm := new(models.NeoSlimeMold)
		//sm.SniffDistance = *sniffDistance
		return sm
	}, 0)

	experiment.InitPheromone("slime", color.RGBA{0x00, 0xfF, 0x00, 0xff})
	experiment.InitPheromone("food", color.RGBA{0xff, 0xff, 0x00, 0xff})
	experiment.OnStep = func(environment *core.Environment, turtles []*core.Turtle, i int) {
		environment.EvaporateAndDiffuse(0.99, "slime")
		environment.EvaporateAndDiffuse(0.99, "food")

		for x := 0; x < 300; x++ {
			for y := 0; y < 300; y++ {
				if maze[y/20][x/20] == 2 && environment.HasValue(x, y) {
					//environment.Mark("food",x,y,256)
				}
			}
		}

		//	fmt.Printf("%d: Turtles: %v\n", environment.Step, len(turtles))

	}
	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
