package main

import (
	"flag"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
	"math/rand"
)

var sniffDistance = flag.Int("sniffDistance", 3, "the distance a turtle can detect pheromone levels from")
var numWoodChips = flag.Int("numWoodChips", 5000, "the number of woodchips in the model")

type WoodChips struct {
	SniffDistance int
	Carrying      bool
}

func (a *WoodChips) Setup(env *core.Environment, t *core.Turtle) {
	t.SetColor(color.RGBA{0x00, 0xFF, 0xFF, 0x88})
}

func (a *WoodChips) Run(env *core.Environment, t *core.Turtle) {
	if a.Carrying {
		if env.HasValue(t.Pos()) {
			for {
				t.Wiggle()
				t.Step(env)
				if !env.HasValue(t.Pos()) {
					env.PutValue(t.Pos())
					a.Carrying = false
					break
				}
			}
		}
	} else {
		if env.HasValue(t.Pos()) {
			env.TakeValue(t.Pos())
			a.Carrying = true
			t.TurnAround()
		}
	}
	t.Wiggle()
	t.Step(env)
}

func mainrun() {
	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()
	experiment.InitEnvironment(func(environment *core.Environment) {
		for x := 0; x < *numWoodChips; x++ {
			environment.PutValue(rand.Intn(environment.Width()), rand.Intn(environment.Height()))
		}

		for x := 200; x < 210; x++ {
			for y := 200; y < 210; y++ {
				environment.PutValue(x, y)
			}
		}
	})
	experiment.InitTurtles(func() core.Actor {
		sm := new(WoodChips)
		sm.SniffDistance = *sniffDistance
		return sm
	})
	experiment.InitPheromone("trail", color.RGBA{0x80, 0xFF, 0x00, 0x00})
	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
