package main

import (
	"flag"
	"fmt"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"github.com/faiface/pixel/pixelgl"
	"github.com/wcharczuk/go-chart"
	"image/color"
	"math"
	"math/rand"
)

var numPrey = flag.Int("numPrey", 300, "the number of prey")

var initialPreyEnergy = flag.Int("initialPreyEnergy", 15, "initial prey energy")
var preyMaxLife = flag.Int("preyMaxLife", 25, "the max lifespan of prey (in simulation steps)")
var preyReproductiveAge = flag.Int("preyReproductiveAge", 4, "the age a prey might reproduce")
var preyReproductionEnergy = flag.Int("preyReproductionEnergy", 4, "energy required for prey reproduction")
var preyReproductionProbability = flag.Float64("preyReproductionProbability", 0.4, "preys probability of reproducing")

var numPred = flag.Int("numPred", 30, "the number of predators")
var initialPredatorEnergy = flag.Int("initialPredatorEnergy", 40, "initial predator energy")
var predatorMaxLife = flag.Int("predatorMaxLife", 60, "the max lifespan of predators (in simulation steps)")
var predatorMaxEnergy = flag.Int("predatorMaxEnergy", 30, "max amount of energy a predator can have")
var predatorReproductionEnergy = flag.Int("predatorReproductionEnergy", 50, "predator reproductive energy")
var predatorReproductionProbability = flag.Float64("predatorReproductionProbability", 0.05, "predators probability of reproducing")

type Predator struct {
	Steps  int
	Energy int
}

func (sm *Predator) Setup(env *core.Environment, t *core.Turtle) {
	// Do nothing
	t.SetAttribute("type", "predator")
	t.SetColor(color.RGBA{255, 200, 0, 0})
	sm.Energy = *initialPredatorEnergy
}

func (sm *Predator) Run(env *core.Environment, t *core.Turtle) {
	sm.Steps++

	if sm.Steps == *predatorMaxLife {
		t.SetAttribute("status", "dead")
		return
	}
	sm.Energy--
	if sm.Energy == 0 {
		t.SetAttribute("status", "dead")
		return
	}
	t.Wiggle()
	prey := t.Check(env)
	if prey != nil {
		if prey.GetAttribute("type") == "prey" && prey.GetAttribute("status") != "dead" {
			prey.SetAttribute("status", "dead")
			sm.Energy = int(math.Max(float64(sm.Energy)+10, float64(*predatorMaxEnergy)))
		}
	}
	t.FollowGradient(env, 3, 1, "scent")
	t.Step(env)
}

type Prey struct {
	Steps  int
	Energy int
}

func (sm *Prey) Setup(env *core.Environment, t *core.Turtle) {
	// Do nothing
	t.SetAttribute("type", "prey")
	t.SetColor(color.RGBA{100, 0, 100, 0})
	sm.Steps = 0
	sm.Energy = *initialPreyEnergy
}

func (sm *Prey) Run(env *core.Environment, t *core.Turtle) {
	sm.Steps++
	sm.Energy--

	if sm.Steps >= *preyMaxLife || sm.Energy == 0 {
		t.SetAttribute("status", "dead")
		return
	}
	t.Wiggle()
	t.Drop(env, 1, "scent")
	t.Step(env)
	if env.HasValue(t.Pos()) {
		env.TakeValue(t.Pos())
		sm.Energy += 1
	}
}

func mainrun() {
	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()
	experiment.InitEnvironment(func(env *core.Environment) {
		for i := 0; i < 1200; i++ {
			x := rand.Intn(env.Width())
			y := rand.Intn(env.Height())
			env.PutValue(x, y)
		}
	})
	experiment.InitNTurtles(func() core.Actor {
		sm := new(Predator)
		return sm
	}, *numPred)
	experiment.InitNTurtles(func() core.Actor {
		sm := new(Prey)
		return sm
	}, *numPrey)

	x := []float64{}
	pred := []float64{}
	prey := []float64{}

	experiment.AddPlot("Predators vs. Prey", func(environment *core.Environment, turtles []*core.Turtle) *chart.Chart {
		preyalive := 0
		predalive := 0

		for _, turtle := range turtles {
			if turtle.GetAttribute("type") == "prey" && turtle.GetAttribute("status") != "dead" {
				preyalive++
				prey := turtle.GetActor().(*Prey)
				if prey.Steps > (*preyReproductiveAge) && prey.Energy > (*preyReproductionEnergy) && float64(rand.Intn(100)) < (100*(*preyReproductionProbability)) {
					experiment.InitNTurtles(func() core.Actor {
						sm := new(Prey)
						return sm
					}, 1)
					preyalive++
				}
			}
			if turtle.GetAttribute("type") == "predator" && turtle.GetAttribute("status") != "dead" {
				predalive++

				if turtle.GetAttribute("type") == "predator" {
					pred := turtle.GetActor().(*Predator)
					if pred.Energy >= (*predatorReproductionEnergy) && float64(rand.Intn(100)) < (100*(*predatorReproductionProbability)) {
						experiment.InitNTurtles(func() core.Actor {
							sm := new(Predator)
							return sm
						}, 1)
						predalive++
					}
				}
			}
		}

		x = append(x, float64(environment.Step))
		prey = append(prey,  100*(float64(preyalive)/float64(environment.Width()*environment.Height())))
		pred = append(pred, 100*(float64(predalive)/float64(environment.Width()*environment.Height())))

		preypop := chart.ContinuousSeries{
			Name:    "Prey Population Density",
			XValues: x,
			YValues: prey,
		}

		predpop := chart.ContinuousSeries{
			Name:    "Predator Population Density",
			XValues: x,
			YValues: pred,

		}

		graph := chart.Chart{

			Background: chart.Style{
				Padding: chart.Box{
					Top: 50,
				},
			},
			XAxis: chart.XAxis{Name: "Time Step", NameStyle: chart.Style{Show: true}, Style: chart.Style{Show: true}, ValueFormatter: func(v interface{}) string {
				return fmt.Sprintf("%d", int(v.(float64)))
			}},
			YAxis: chart.YAxis{Name: "Population", NameStyle: chart.Style{Show: true}, Style: chart.Style{Show: true,}, ValueFormatter: func(v interface{}) string {
				return fmt.Sprintf("%d", int(v.(float64)))
			}},
			Series: []chart.Series{
				preypop,
				predpop,
			},
		}
		return &graph
	})

	experiment.InitPheromone("scent", color.RGBA{0x80, 0xFF, 0x00, 0x00})
	fmt.Printf("%v, %v,%v\n", "time step", "number of prey", "number of predators")
	experiment.OnStep = func(env *core.Environment, turtles []*core.Turtle, step int) {
		env.EvaporateAndDiffuse(0.95, "scent")
		// Grow Grass
		x := rand.Intn(env.Width())
		y := rand.Intn(env.Height())
		env.PutValue(x, y)

		//if step == 0 {
		//fmt.Printf("%v,%v,%v\n", step, alive, predalive)
		//}
	}
	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
