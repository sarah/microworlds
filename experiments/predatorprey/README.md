# Predator Prey Model

Models interactions between prey who eat grass, and predators who eat prey.

![](oscillations.png)
