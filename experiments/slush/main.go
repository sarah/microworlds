package main

import (
	"flag"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
	"math/rand"
	"strconv"
)

var prob = flag.Float64("distribution", .51, "drives the probability of color assignment of turtles in consensus models, closer to .5 results in more even assignment, closer to 0 or 1 biases in favor of a color.")

type Slush struct {
	color       int
	Probability float64
}

func NewSlushTurtle() core.Actor {
	turtle := new(Slush)
	turtle.Probability = *prob
	return turtle
}

func (sm *Slush) Setup(env *core.Environment, t *core.Turtle) {
	num := rand.Intn(100)
	if num >= int(sm.Probability*100.0) {
		sm.color = 2
	} else {
		sm.color = 1
	}
}

func (sm *Slush) Run(env *core.Environment, t *core.Turtle) {
	t.Wiggle()
	am1 := t.Amount(env, 1, "1")
	am2 := t.Amount(env, 1, "2")

	t.Drop(env, 1, strconv.Itoa(sm.color))

	if am1 > 3 || am2 > 3 {
		if am1 > am2 {
			sm.color = 1
		} else if am2 > am1 {
			sm.color = 2
		}
	}
	t.Step(env)
}

func mainrun() {
	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()
	experiment.InitTurtles(NewSlushTurtle)
	experiment.InitPheromone("1", color.RGBA{0x80, 0xFF, 0x00, 0x00})
	experiment.InitPheromone("2", color.RGBA{0xFF, 0x00, 0xFF, 0x00})
	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
