package main

import (
	"flag"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
)

var sniffDistance = flag.Int("sniffDistance", 5, "the distance a turtle can detect pheromone levels from")

type MazeGeneration struct {
	SniffDistance int
}

func (sm *MazeGeneration) Setup(env *core.Environment, t *core.Turtle) {
	// Do nothing
}

func (sm *MazeGeneration) Run(env *core.Environment, t *core.Turtle) {
	t.Wiggle()
	t.FollowGradient(env, sm.SniffDistance, 0, "trail")
	t.TurnAround() // Run away from the strongest trail
	t.Step(env)
	t.Drop(env, 1, "trail")
}

func mainrun() {
	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()
	experiment.InitTurtles(func() core.Actor {
		sm := new(MazeGeneration)
		sm.SniffDistance = *sniffDistance
		return sm
	})
	experiment.InitPheromone("trail", color.RGBA{0x81, 0x00, 0x81, 0x00})
	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
