package main

import (
	"flag"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
	"math/rand"
	"strconv"
)

var prob = flag.Float64("distribution", .51, "drives the probability of color assignment of turtles in consensus models, closer to .5 results in more even assignment, closer to 0 or 1 biases in favor of a color.")
var byzantineTurtles = flag.Int("byzantineTurtles", 50, "in consensus simlulations, the number of turtles who will always vote 2")

type Isolate struct {
	color       int
	Probability float64
	sureness    float32
	Byztantine  bool
}

func (sm *Isolate) Setup(env *core.Environment, t *core.Turtle) {
	num := rand.Intn(100)
	if num >= int(sm.Probability*100.0) {
		sm.color = 2
	} else {
		sm.color = 1
	}
	sm.sureness = 2
}

func (sm *Isolate) Run(env *core.Environment, t *core.Turtle) {
	if sm.Byztantine {
		t.Wiggle()
		t.FollowGradient(env, 10, 0, "2")
		t.Drop(env, 1, "2")
		t.Step(env)
	} else {
		t.Wiggle()
		am1 := t.Amount(env, 1, "1")
		am2 := t.Amount(env, 1, "2")

		if am1 > sm.sureness || am2 > sm.sureness {
			if am1 > am2 {
				if sm.color == 2 {
					sm.sureness--
				} else {
					sm.sureness++
				}
				if sm.sureness == 0 {
					sm.color = 1
				}
			} else if am2 > am1 {
				if sm.color == 1 {
					sm.sureness--
				} else {
					sm.sureness++
				}
				if sm.sureness == 0 {
					sm.color = 2
				}
			}
		}

		if sm.sureness > 1 {
			t.Drop(env, 1, strconv.Itoa(sm.color))
		}

		t.Step(env)
	}
}

func mainrun() {
	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()
	honestTurtles := experiment.GetNumTurtles() - (*byzantineTurtles)
	experiment.InitNTurtles(func() core.Actor {
		sm := new(Isolate)
		sm.Probability = *prob
		return sm
	}, honestTurtles)
	experiment.InitNTurtles(func() core.Actor {
		sm := new(Isolate)
		sm.Probability = *prob
		sm.Byztantine = true
		return sm
	}, (*byzantineTurtles))
	experiment.InitPheromone("1", color.RGBA{0x00, 0xFF, 0x00, 0x00})
	experiment.InitPheromone("2", color.RGBA{0xFF, 0x00, 0xFF, 0x00})
	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
