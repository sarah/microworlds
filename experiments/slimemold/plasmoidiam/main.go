package main

import (
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"git.openprivacy.ca/sarah/microworlds/models"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
)

func mainrun() {
	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()

	num := 0

	experiment.InitEnvironment(func(environment *core.Environment) {
		x, y := 120, 120
		for i := 0; i < 20; i++ {
			for j := 0; j < 20; j++ {
				environment.PutValue(x+i, y+j)
			}
		}

		x, y = 250, 120
		for i := 0; i < 20; i++ {
			for j := 0; j < 20; j++ {
				environment.PutValue(x+i, y+j)
			}
		}

		x, y = 160, 130
		for i := 0; i < 10; i++ {
			for j := 0; j < 10; j++ {
				environment.PutValue(x+i, y+j)
			}
		}
	})

	experiment.InitNTurtles(func() core.Actor {
		sm := new(models.SlimeMold)
		sm.Num = num
		num++
		return sm
	}, 10000)

	experiment.InitPheromone("slime", color.RGBA{0x80, 0xFF, 0x00, 0x00})
	experiment.InitPheromone("food", color.RGBA{0xff, 0x00, 0x00, 0x00})
	experiment.OnStep = func(environment *core.Environment, turtles []*core.Turtle, i int) {
		environment.EvaporateAndDiffuse(0.99, "slime")
		environment.EvaporateAndDiffuse(0.99, "food")
	}
	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
