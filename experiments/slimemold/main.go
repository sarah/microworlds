package main

import (
	"flag"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
)

var sniffDistance = flag.Int("sniffDistance", 3, "the distance a turtle can detect pheromone levels from")
var defensiveDecentralization = flag.Bool("defensiveDecentralization", false, "if true, slime molds will break up if the concentration is too great")

type SlimeMold struct {
	SniffDistance  int
	StartX, StartY int
}

func (sm *SlimeMold) Setup(env *core.Environment, t *core.Turtle) {
	t.SetColor(color.RGBA{100, 255, 10, 0})

}

func (sm *SlimeMold) Run(env *core.Environment, t *core.Turtle) {
	t.Wiggle()
	if *defensiveDecentralization == false {
		t.FollowGradient(env, sm.SniffDistance, 2, "trail")
	} else if t.Amount(env, sm.SniffDistance, "trail") < 5.2 {
		t.FollowGradient(env, sm.SniffDistance, 2, "trail")
	} else {
		//t.FollowGradient(env, sm.SniffDistance, 2, "trail")
		//t.TurnAround()
	}
	t.Step(env)
	t.Drop(env, 1, "trail")
}

func mainrun() {
	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()
	experiment.InitTurtles(func() core.Actor {
		sm := new(SlimeMold)
		sm.SniffDistance = *sniffDistance
		return sm
	})
	experiment.InitPheromone("trail", color.RGBA{0x80, 0xFF, 0x00, 0x00})

	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
