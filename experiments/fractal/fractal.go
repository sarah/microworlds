package main

import (
	"flag"
	"git.openprivacy.ca/sarah/microworlds/core"
	"git.openprivacy.ca/sarah/microworlds/experiments"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
	"math"
)

var sniffDistance = flag.Int("sniffDistance", 3, "the distance a turtle can detect pheromone levels from")
var defensiveDecentralization = flag.Bool("defensiveDecentralization", false, "if true, slime molds will break up if the concentration is too great")

type SlimeMold struct {
	SniffDistance int
	Age           int
	Stuck         bool
}

func (sm *SlimeMold) Setup(env *core.Environment, t *core.Turtle) {
	t.SetColor(color.RGBA{100, 255, 10, 0})
}

func (sm *SlimeMold) Run(env *core.Environment, t *core.Turtle) {
	frac := float64(sm.Age) / math.Min(float64(env.Step+1), 255)
	col := uint8(256 * frac)
	if env.Step < 100 {
		t.SetColor(color.RGBA{col, 0, col / 2, 0xf2})
	} else {
		t.SetColor(color.RGBA{col, 0, col, col})
	}
	if sm.Stuck == false {
		sm.Age++
		//t.Wiggle()
		//t.Wiggle()
		t.Wiggle()
		t.Step(env)
		c := t.Check(env)

		if c != nil {
			s := c.GetActor().(*SlimeMold)
			if s.Stuck == true {
				sm.Stuck = true
			}
		}
	}
}

func mainrun() {
	experiment := new(experiments.Experiment)
	experiment.InitializeExperiment()
	n := 0
	experiment.InitTurtles(func() core.Actor {
		n++
		sm := new(SlimeMold)
		if n == 1 {
			sm.Stuck = true
		}
		return sm
	})
	experiment.InitPheromone("trail", color.RGBA{0x80, 0xFF, 0x00, 0x00})

	experiment.Run()
}

func main() {
	pixelgl.Run(mainrun)
}
