package models

import (
	"git.openprivacy.ca/sarah/microworlds/core"
	"image/color"
)

type SlimeMold struct {
	Num            int
	StartX, StartY int
	N              int
}

func (sm *SlimeMold) Setup(env *core.Environment, t *core.Turtle) {
	t.SetColor(color.RGBA{100, 255, 10, 0})
	if sm.StartX != 0 && sm.StartY != 0 {
		t.SetXY(sm.StartX, sm.StartY)
	}
}

func (sm *SlimeMold) CheckNeighbours(env *core.Environment, ox, oy int) int {
	neighbours := 0
	if env.Check(ox-1, oy-1) {
		neighbours++
	}
	if env.Check(ox, oy-1) {
		neighbours++
	}
	if env.Check(ox+1, oy-1) {
		neighbours++
	}
	if env.Check(ox-1, oy) {
		neighbours++
	}
	if env.Check(ox+1, oy) {
		neighbours++
	}

	if env.Check(ox-1, oy+1) {
		neighbours++
	}
	if env.Check(ox, oy+1) {
		neighbours++
	}
	if env.Check(ox+1, oy+1) {
		neighbours++
	}
	return neighbours
}

func (sm *SlimeMold) Run(env *core.Environment, t *core.Turtle) {

	// Move around the world, if there are too many slimes around us turn around, otherwise follow slimes and food.
	t.Wiggle()
	if t.Amount(env, 1, "slime") > 10 {
		t.TurnAround()
	} else {
		t.FollowGradient(env, 1, 1, "slime")
	}
	t.FollowGradient(env, 1, .1, "food")

	// If we have no neighbours we pretend we found some food so the others can find us.
	if t.Step(env) {
		ox, oy := t.Pos()
		if sm.CheckNeighbours(env, ox, oy) > 3 && !env.HasValue(t.Pos()) {
			t.Drop(env, .1, "slime")
		} else {
			t.Drop(env, 1, "food")
		}
	} else {
		// We are on top of other slime, add some more randomness
		t.Wiggle()
		t.Wiggle()
		t.Wiggle()
	}

	// We've found food, let's drop some chemical to tell others
	if env.HasValue(t.Pos()) {
		env.TakeValue(t.Pos())
		t.Drop(env, 10, "food")
	}
}
