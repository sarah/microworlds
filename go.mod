module git.openprivacy.ca/sarah/microworlds

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/asim/quadtree v0.0.0-20190907063054-ae2e556e6bb4
	github.com/faiface/glhf v0.0.0-20181018222622-82a6317ac380 // indirect
	github.com/faiface/mainthread v0.0.0-20171120011319-8b78f0a41ae3 // indirect
	github.com/faiface/pixel v0.8.0
	github.com/foolusion/quadtree v0.0.0-20140826014210-88d124c993be
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7 // indirect
	github.com/go-gl/glfw v0.0.0-20190409004039-e6da0acd62b1 // indirect
	github.com/go-gl/mathgl v0.0.0-20190713194549-592312d8590a // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/nickng/bibtex v1.0.1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/veandco/go-sdl2 v0.3.3
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8 // indirect
)
